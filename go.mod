module gitlab.com/go-lang-tools/events

go 1.15

require (
	gitlab.com/go-lang-tools/tools v0.0.1
	github.com/confluentinc/confluent-kafka-go v1.8.2
	github.com/google/uuid v1.3.0
	github.com/jackc/pgtype v1.8.1
	github.com/rs/zerolog v1.25.0
	github.com/streadway/amqp v1.0.0
)
