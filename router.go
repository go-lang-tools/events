package events

import "github.com/streadway/amqp"

const headerRouteKey = "type"

// Router отпределяет путь для сообщения
type Router interface {
	GetRoute(msg amqp.Delivery) string
}

// HeaderRouter отпределяет путь по заголовку
// По умолчанию смотрит на зоголовок с ключом type
type HeaderRouter struct {
	key string
}

func NewHeaderRouter() *HeaderRouter {
	return &HeaderRouter{
		key: headerRouteKey,
	}
}

// SetKey указывает в каком заголовке смотреть путь для сообщения
func (r *HeaderRouter) SetKey(key string) {
	if key != "" {
		r.key = key
	}
}

func (r *HeaderRouter) GetRoute(msg amqp.Delivery) string {
	var route string

	if typeFromHeader, ok := msg.Headers[headerRouteKey]; ok {
		route = typeFromHeader.(string)
	}

	return route
}

// RouterKeyRouter отпределяет путь по заголовку routing key
type RouterKeyRouter struct {
}

func NewRouterKeyRouter() *RouterKeyRouter {
	return &RouterKeyRouter{}
}

func (r *RouterKeyRouter) GetRoute(msg amqp.Delivery) string {
	return msg.RoutingKey
}
